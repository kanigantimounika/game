function game(choice)
{
    var humanchoice=choice.id
    var computerchoice=getSelection(getRandomNumber());
    
    reset();
    getImages(humanchoice,computerchoice);

}
function getRandomNumber()
{
    return Math.floor(Math.random()*3);
}
function getSelection(choice)
{
    var selectionrep=["rock","paper","scissors"];
    return selectionrep[choice];
}
function getImages(humanchoice,computerchoice)
{

    var imgrepository={
        "rock":document.getElementById("rock").src,
        "paper":document.getElementById("paper").src,
        "scissors":document.getElementById("scissors").src

    }
    
  
   var img1=document.createElement("div");
   img1.setAttribute("id","image1");
   img1.innerHTML="<img src='" +imgrepository[humanchoice] +"' height=150 width=150 >"
   document.getElementById("imagegetter").appendChild(img1);
   var img2=document.createElement("div");
   img2.setAttribute("id","image2");
   img2.innerHTML="<img src='" +imgrepository[computerchoice] +"' height=150 width=150 >"
   document.getElementById("imagegetter").appendChild(img2);




}

function reset() {
    let humanSelection = document.getElementById('image1');
    let computerSelection = document.getElementById('image2');

    humanSelection?.remove();
    computerSelection?.remove();
}
